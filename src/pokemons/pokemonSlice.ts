import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Pokemon, NamedAPIResource } from 'pokenode-ts';
import { fetchPage, fetchPokemon } from './pokemonAPI';
import _ from 'lodash'

export interface PokeState {
  list: NamedAPIResource[],
  offset: number,
  limit: number,
  next: string | null,
  chosenPokemon: Pokemon | null,
  hasMore: boolean,
  loading: 'idle' | 'pending' | 'succeeded' | 'failed'
}

const initialState: PokeState = {
  list: [],
  next: null,
  offset: 0,
  limit: 200,
  chosenPokemon: null,
  hasMore: true,
  loading: 'idle'
};

export const fetchPageAsync = createAsyncThunk(
  'pokemon/fetchPage',
  async (inputs: { offset: number, limit: number }) => {
    const response = await fetchPage(inputs.offset, inputs.limit);
    return response;
  }
);

export const fetchPokemonAsync = createAsyncThunk(
  'pokemon/fetchPokemon',
  async (inputs: { name: string }) => {
    const response = await fetchPokemon(inputs.name);
    return response;
  }
);

export const pokemonSlice = createSlice({
  name: 'pokemon',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setLimit: (state, action: PayloadAction<number>) => {
      state.limit = action.payload
    },
  },
  extraReducers: (builder) => {

    builder.addCase(
      fetchPokemonAsync.pending,
      (state, action) => { state.loading = 'pending' }
    )

    builder.addCase(
      fetchPokemonAsync.rejected,
      (state, action) => {
        state.loading = 'failed'
      }
    )

    builder.addCase(
      fetchPokemonAsync.fulfilled,
      (state, action) => {
        state.chosenPokemon = action.payload || null
        state.loading = 'succeeded'
      }
    )


    builder.addCase(
      fetchPageAsync.pending,
      (state, action) => { state.loading = 'pending' }
    )

    builder.addCase(
      fetchPageAsync.rejected,
      (state, action) => {
        state.loading = 'failed'
      }
    )

    builder.addCase(
      fetchPageAsync.fulfilled,
      (state, action) => {
        state.list = _.uniqBy([...state.list, ...action.payload], res => res.url)
        state.offset = state.list.length
        state.hasMore = action.payload.length > 0
        state.loading = 'succeeded'
      }
    )
  }
});

export const { setLimit } = pokemonSlice.actions;
export default pokemonSlice.reducer;
