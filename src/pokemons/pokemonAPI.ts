import { Pokemon, PokemonClient } from 'pokenode-ts'

export const pokemonClient = new PokemonClient();

export async function fetchPage(offset = 0, limit = 20) {
  const result = await pokemonClient.listPokemons(offset, limit)
  return result.results;
}

const memo = new Map<string, Pokemon>()

export async function fetchPokemon(name: string) {
  if (memo.has(name)) return memo.get(name)

  const result = await pokemonClient.getPokemonByName(name)
  memo.set(name, result)
  return result
}