import {
  Button, Stack, CssBaseline,
  Dialog, DialogTitle, DialogContent, TextField, DialogActions, Grid, LinearProgress,

  // The idea had potential XDD
  //Autocomplete
} from '@mui/material';
import { Container } from '@mui/system';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { useAppSelector, useAppDispatch } from '../app/hooks';
import PokemonDetails from './PokemonDetails';
import {
  fetchPageAsync,
  fetchPokemonAsync,
  // Planned to use this to control the pagination amount at each load
  // But turned out to be unecessary
  // setLimit
} from './pokemonSlice';

function Pokemon() {
  const dispatch = useAppDispatch();
  const state = useAppSelector(state => state.pokemons)
  const [open, setOpen] = useState(false)
  const [filter, setFilter] = useState('')

  // This is just a last minute idea hacked together
  // Plz don't judge XDDD
  const preg = new RegExp(`.*${filter.split('').join('.*')}.*`, 'igm')

  const handlePokeClick = (name: string) => {
    return () => {
      dispatch(
        fetchPokemonAsync({ name })
      )
      setOpen(true)
    }
  }

  const handleNext = () => {
    if (state.loading !== 'pending')
      dispatch(
        fetchPageAsync({ offset: state.offset + state.limit, limit: state.limit })
      )
  }

  return (
    <Container>
      <CssBaseline />

      <datalist id='pokemon-names'>
        {
          state.list.map(p => <option value={p.name} />)
        }
      </datalist>

      <Stack sx={{ padding: 3 }}>
        <TextField
          label="Search"
          autoFocus={true}
          placeholder='search...'
          onChange={(e) => setFilter(e.target.value)}
        />
        {/*
          Sounded cool in my head but performed bad :)
          <Autocomplete
            id='pokemon-names'
            options={state.list.map(p => p.name)}
            onChange={(_, newValue) => setFilter(newValue || '')}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Search"
                autoFocus={true}
                placeholder='search...'
              />)}
          />
        */}
      </Stack>

      <InfiniteScroll
        loadMore={handleNext}
        hasMore={state.hasMore}
        loader={<LinearProgress />}
      >
        <Grid container direction='row' columns={4} justifyItems='center' rowSpacing={2} sx={{ padding: 3 }} id='scGrid' >
          {state.list.filter(p => preg.test(p.name)).map(pokemon => (

            <Grid item xs={1}>
              <Button variant='outlined' onClick={handlePokeClick(pokemon.name)} size='large' key={pokemon.url}>
                {pokemon.name}
              </Button>
            </Grid>

          ))}
        </Grid>
      </InfiniteScroll>


      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>{state.loading === 'pending' ? 'Loading...' : `${state.chosenPokemon?.name} details`}</DialogTitle>
        <DialogContent>
          <PokemonDetails />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)}>Close</Button>
        </DialogActions>
      </Dialog>
    </Container >
  );
}

export default Pokemon