import { Stack, LinearProgress } from '@mui/material'
import { useAppSelector } from '../app/hooks'

function PokemonDetails() {
    const { chosenPokemon: pokemon, loading } = useAppSelector(state => state.pokemons)

    if (loading !== 'pending' && pokemon === null)
        return (<h1>Something went wrong loading the pokemon</h1>)
    else if (loading === 'pending')
        return (<LinearProgress />)
    else if (pokemon !== null) // This test isn't really needed but TS complains and i hate warnings XDD
        return (
            <Stack>
                <h1>Pokemon: {pokemon.name} ({pokemon.id})</h1>

                <div>
                    <h3>Species: {pokemon.species.name}</h3>
                    <h3>Base XP: {pokemon.base_experience}</h3>
                    <h3>Height: {pokemon.height}</h3>
                    <h3>Weight: {pokemon.weight}</h3>
                    <h3>Forms: {pokemon.forms.map(form => form.name).join(', ')}</h3>
                </div>

                <h3>Moves: {pokemon.moves.map(move => move.move.name).join(', ')}</h3>

                <div>
                    <h3>Stats:</h3>
                    {
                        pokemon.stats.map(stat => (<h4>{stat.stat.name} - {stat.base_stat} - {stat.effort}</h4>))
                    }
                </div>

                <div>
                    <h3>Abilities:</h3>
                    {
                        pokemon.abilities.map(ability => (<h4>{ability.ability.name} ({ability.is_hidden ? 'Hidden' : 'Not hidden'})</h4>))
                    }
                </div>
            </Stack>
        )

    return <h1>Weird</h1>
}

export default PokemonDetails