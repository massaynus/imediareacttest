FROM node:latest AS build-env

WORKDIR /app

COPY package.json .
RUN npm install

COPY . .
RUN ls -lh
RUN npm run build

FROM node:latest
WORKDIR /app
RUN npm i -g serve

COPY --from=build-env /app/build .

ENTRYPOINT ["serve", "-s", "/app"]
